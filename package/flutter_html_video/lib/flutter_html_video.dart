library flutter_html_video;

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:video_player/video_player.dart';
import 'package:html/dom.dart' as dom;
import 'dart:io';

/// [VideoHtmlExtension] adds support for the <video> tag to the flutter_html
/// library.
class VideoHtmlExtension extends HtmlExtension {
  final VideoControllerCallback? videoControllerCallback;

  const VideoHtmlExtension({
    this.videoControllerCallback,
  });

  @override
  Set<String> get supportedTags => {"video"};

  @override
  InlineSpan build(ExtensionContext context) {
    return WidgetSpan(
        child: VideoWidget(
      context: context,
      callback: videoControllerCallback,
    ));
  }
}

typedef VideoControllerCallback = void Function(
    dom.Element?, ChewieController, VideoPlayerController);

/// A VideoWidget for displaying within the HTML tree.
class VideoWidget extends StatefulWidget {
  final ExtensionContext context;
  final VideoControllerCallback? callback;
  final List<DeviceOrientation>? deviceOrientationsOnEnterFullScreen;
  final List<DeviceOrientation> deviceOrientationsAfterFullScreen;

  const VideoWidget({
    Key? key,
    required this.context,
    this.callback,
    this.deviceOrientationsOnEnterFullScreen,
    this.deviceOrientationsAfterFullScreen = DeviceOrientation.values,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _VideoWidgetState();
}

class _VideoWidgetState extends State<VideoWidget> {
  ChewieController? _chewieController;
  VideoPlayerController? _videoController;

  @override
  void initState() {
    final attributes = widget.context.attributes;

    final sources = <String?>[
      if (attributes['src'] != null) attributes['src'],
      ...ReplacedElement.parseMediaSources(widget.context.node.children),
    ];
    // final givenWidth = double.tryParse(attributes['width'] ?? "");
    // final givenHeight = double.tryParse(attributes['height'] ?? "");
    if (sources.isNotEmpty && sources.first != null) {
    // double mWidth = givenWidth ?? (givenHeight ?? 150) * 2;
    // double mHeight = givenHeight ?? (givenWidth ?? 300) / 2;
      Uri sourceUri = Uri.parse(sources.first!);
      switch (sourceUri.scheme) {
        case 'asset':
          _videoController = VideoPlayerController.asset(sourceUri.path);
          break;
        case 'file':
          _videoController =
              VideoPlayerController.file(File.fromUri(sourceUri));
          break;
        default:
          _videoController =
              VideoPlayerController.networkUrl(Uri.parse(sourceUri.toString()));
          break;
      }

      _videoController?.initialize().then((value){

        _chewieController = ChewieController(
          videoPlayerController: _videoController!,
          placeholder:
              attributes['poster'] != null && attributes['poster']!.isNotEmpty
                  ? Image.network(attributes['poster']!)
                  : Container(color: Colors.black),
          autoPlay: attributes['autoplay'] != null,
          looping: attributes['loop'] != null,
          showControls: attributes['controls'] != null,
          autoInitialize: true,
          allowFullScreen: false,
          aspectRatio:_videoController?.value.aspectRatio??1,
          deviceOrientationsOnEnterFullScreen:
              widget.deviceOrientationsOnEnterFullScreen,
          deviceOrientationsAfterFullScreen:
              widget.deviceOrientationsAfterFullScreen,
        );
        widget.callback?.call(
          widget.context.element,
          _chewieController!,
          _videoController!,
        );

        setState(() {
        });
      });
    }
    _videoController?.addListener(_addListener);
    super.initState();
  }

  @override
  void dispose() {
    _videoController?.removeListener(_addListener);
    _chewieController?.dispose();
    _videoController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext bContext) {
    if (_chewieController == null) {
      return const DefaultVideoView();
    }
return  (_videoController?.value.isInitialized??false)?AspectRatio(aspectRatio: _videoController?.value.aspectRatio??1,child: Chewie(
        controller: _chewieController!,
      ),):const DefaultVideoView();
  }

  Future<void> _addListener() async {

  }
}

class DefaultVideoView extends StatelessWidget {
  const DefaultVideoView({Key? key}):super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      height: width * 9.0 / 16.0,
      color: Colors.black
    );
  }
}